package main.cart.repository;

import main.cart.model.Product;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import java.util.List;

class ProductRepositoryTest {

    ProductRepository productRepositoryTest = new ProductRepository();

    @Test
    void itShouldFetchProductInfo() {

        // Given
        long productID = 101L;
        Product productTest = new Product(productID, "beef", 6.90);

        // When
        Product productFetched = productRepositoryTest.fetchProductInfo(productID);

        // Then
        assertThat(productFetched).isNotNull().usingRecursiveComparison().isEqualTo(productTest);
    }

    @Test
    void itShouldNotFetchWhenIdNotValid() {

        // Given
        long productID = 104L;

        // When
        Product productFetched = productRepositoryTest.fetchProductInfo(productID);

        // Then
        assertThat(productFetched).isNull();

    }


    @Test
    void itShouldNotAddToCartWhenIdNotValid() {

        // Given
        long productId = 104L;

        // When
//        productRepositoryTest.addProduct(productId);

        // Then
        assertThatThrownBy(() -> productRepositoryTest.addProduct(productId))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("Product ID: " + productId + " is not valid!");
        assertThat(productRepositoryTest.cart).isEmpty();

    }

    @Test
    void itShouldEmptyCart() {

        // Given
        productRepositoryTest.addProduct(101L);

        // When
        productRepositoryTest.emptyCart();

        // Then
        assertThat(productRepositoryTest.cart).isEmpty();
    }

    @Test
    void itShouldGetTotalPrice() {

        // Given
        productRepositoryTest.addProduct(101L);
        productRepositoryTest.addProduct(102L);

        // When
        double price = productRepositoryTest.getTotalPrice();

        // Then
        assertThat(price).isEqualTo(14.1, offset(0.1));
    }


    @Test
    void itShouldRemoveProduct() {

        // Given
        long productId = 101L;
        productRepositoryTest.addProduct(productId);

        // When
        productRepositoryTest.removeProduct(productId);

        // Then
        assertThat(productRepositoryTest.cart).isEmpty();
    }

    @Test
    void itShouldThrowErrorWhenRemoveItemNotAvailable() {

        // Given
        long productID = 101L;

        // When
        // Then
        assertThatThrownBy(() -> productRepositoryTest.removeProduct(101L))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("Item with ID: " + productID + " not available in the cart!");
    }

    @Test
    void itShouldAddProduct() {

        // Given
        long productId = 101L;
        Product product = new Product(101, "beef", 6.90);

        // When
        productRepositoryTest.addProduct(productId);

        // Then
        assertThat(productRepositoryTest.cart).isNotEmpty().hasSize(1).contains(product);

    }

    @Test
    void itShouldGetAllProducts() {
        // Given
        long productId = 101L;
        List<Product> cartTest = List.of(
                new Product(101, "beef", 6.90),
                new Product(101, "beef", 6.90)
        );
        productRepositoryTest.addProduct(productId);
        productRepositoryTest.addProduct(productId);

        // When
        List<Product> cart = productRepositoryTest.getAllProducts();

        // Then
        assertThat(cart).isNotEmpty().hasSize(2).containsAll(cartTest);
    }
}