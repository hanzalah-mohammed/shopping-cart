package main.cart.repository;

import main.cart.model.Product;

import java.util.*;

public class ProductRepository {

    // Sample product list which acts like a database
    List<Product> productDatabase = List.of(
            new Product(101, "beef", 6.90),
            new Product(102, "chicken", 7.20),
            new Product(103, "fish", 8.60)
    );

    // Using the productID to fetch product from the productDatabase
    public Product fetchProductInfo(long productIdInput){
        for (Product product : productDatabase){ // iterate through all the products in the database,
            if (productIdInput == product.getProductID()){ // if it matches with productID from the user input,
                return product; // then return the whole product.
            }
        }
        return null;
    }

    // create empty cart list
    List<Product> cart = new ArrayList<>();

    public void addProduct(long productIdInput){

        // fetch the product using the productID
        Product productToBeAdded = fetchProductInfo(productIdInput);

//        try {
            if (productToBeAdded != null) {
                cart.add(productToBeAdded); // add the fetched product in the cart
            } else {
                throw new IllegalStateException("Product ID: " + productIdInput + " is not valid!");
            }
//        }
//        catch (IllegalStateException e) {
//            System.err.println(e.getMessage());
//        }
    }

    public List<Product> getAllProducts(){
        return cart;
    }

    public double getTotalPrice(){
        return cart.stream().mapToDouble(Product::getPrice).sum();
    }

    public void removeProduct(long productIdInput){

        // fetch the product using the productID
        Product productToBeRemoved = fetchProductInfo(productIdInput);
        long count = cart.stream().filter(e -> e.equals(productToBeRemoved)).count();

//        try {
            if (productToBeRemoved != null && count > 0) {
                cart.remove(productToBeRemoved);
            } else {
                throw new IllegalStateException("Item with ID: " + productIdInput + " not available in the cart!");
            }
//        }
//        catch (IllegalStateException e) {
//            System.err.println(e.getMessage());
//        }
    }

    public void emptyCart() {
        System.out.println(cart.size() + " items in the cart have been removed!");
        cart.clear(); // clear all the items in cart list
    }
}
