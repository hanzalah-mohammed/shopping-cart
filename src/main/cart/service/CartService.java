package main.cart.service;

import main.cart.model.Product;
import main.cart.repository.ProductRepository;

import java.text.NumberFormat;
import java.util.*;

public class CartService {
    Scanner scanner = new Scanner(System.in);
    ProductRepository productRepository = new ProductRepository();

    public void addProduct(){
        System.out.println("Enter Product ID:");
        long productID = scanner.nextInt();
        productRepository.addProduct(productID);
    }

    // to map quantity with the product
    // key : value -> product : quantity
    public void viewCart(){

        // get the cart list
        List<Product> cart =  productRepository.getAllProducts();

        // create new cartMap to count quantity for each product
        Map<Product, Integer> cartMap = new HashMap<>();

        if(cart.size() > 0){
            for (Product product : cart){
                if(cartMap.containsKey(product)){
                    cartMap.put(product, cartMap.get(product) + 1); // add the quantity if the product is already exist
                } else {
                    cartMap.put(product, 1);
                }
            }

            System.out.println("\nYour Cart\n----------------------");
            for (Map.Entry<Product, Integer> entry : cartMap.entrySet()){
                System.out.println(entry.getKey() + ", quantity: " + entry.getValue());
            }

            // getTotalPrice() and display it
            double totalPrice = productRepository.getTotalPrice();
            NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US")); // to format the number
            System.out.println("----------------------\nTotal price: $" + nf.format(totalPrice));

        } else {
            System.out.println("Your cart is empty!!");
        }
    }

    public void removeProduct(){
        System.out.println("Enter Product ID:");
        long productID = scanner.nextInt();
        productRepository.removeProduct(productID);
    }

    private void emptyCart() {
        productRepository.emptyCart();
    }

    public void getUserChoice() {

        boolean sessionActive = true;

        while (sessionActive) {
            System.out.println("\n1) Add Item \n2) View Cart \n3) Remove Item \n4) Checkout \n5) Empty Cart \n6) Exit");
            System.out.println("----------------------");
            System.out.println("Enter choice:");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    addProduct();
                    break;
                case 2:
                    viewCart();
                    break;
                case 3:
                    removeProduct();
                    break;
                case 4:
                    viewCart();
                    sessionActive = false;
                    break;
                case 5:
                    emptyCart();
                    break;
                case 6:
                    sessionActive = false;
                    System.out.println("Thank you for shopping with us!");
                    break;
            }
        }
    }
}
