package main.cart.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class ProductTest {

    @Test
    void itShouldGetProductID() {
        // Given
        Product product = new Product(101L, "Rice", 9.99);
        // When
        long id = product.getProductID();
        // Then
        assertThat(id).isEqualTo(101L);
    }

    @Test
    void itShouldGetName() {
        // Given
        Product product = new Product(101L, "Rice", 9.99);
        // When
        String name = product.getName();
        // Then
        assertThat(name).isEqualTo("Rice");
    }

    @Test
    void itShouldGetPrice() {
        // Given
        Product product = new Product(101L, "Rice", 9.99);
        // When
        double price = product.getPrice();
        // Then
        assertThat(price).isEqualTo(9.99);
    }

    @Test
    void itShouldTestToString() {
        // Given
        Product product = new Product(101L, "Rice", 9.99);
        // When
        String string = product.toString();
        // Then
        assertThat(string).isEqualTo("ID: " + product.getProductID() +
                " | Name: " + product.getName() + ", Price: $" + product.getPrice());
    }


}