package main.cart.model;

import java.util.Objects;

public class Product {
    private final long productID;
    private final String name;
    private final double price;

    public Product(long productID, String name, double price) {
        this.productID = productID;
        this.name = name;
        this.price = price;
    }

    public Long getProductID() {
        return productID;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ID: " + productID + " | Name: " + name + ", Price: $" + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productID == product.productID && Double.compare(product.price, price) == 0 && name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, name, price);
    }
}
